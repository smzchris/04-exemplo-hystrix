package br.com.itau.filmes;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FilmesController {

	@GetMapping
	public String getFilme() {
		return "Eu quero ser John Malkovich.";
	}
}
